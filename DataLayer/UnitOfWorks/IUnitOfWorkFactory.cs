﻿using NHibernate;
using NHibernate.Cfg;

namespace DataLayer.UnitOfWorks
{
    public interface IUnitOfWorkFactory
    {
        string DbName { get; set; }

        Configuration Configuration { get; }
        ISessionFactory SessionFactory { get; }
        ISession CurrentSession { get; set; }
        IUnitOfWork Create();
        void DisposeUnitOfWork(UnitOfWorkImplementor adapter);
    }
}
