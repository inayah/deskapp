﻿using InayahStore.Helper;
using InayahStore.Views;
using Models.Domain;
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace InayahStore
{
    public partial class AdminForm : Form, AppEvent
    {
        int PanelWidth;
        bool isCollapsed;
        private bool mouseDown;
        private Point lastLocation;

        private Sales sales;
        private Purchases purchases;
        private MasterData master;
        private Settings settings;
        private Reports reports;
        private Dashboard dashboard;

        public LoginForm LoginForm { get; set; }

        public AdminForm()
        {
            InitializeComponent();
            InitTheme();

            timerTime.Start();
            isCollapsed = false;
            PanelWidth = panelLeft.Width;

            AddControlsToPanel(new Dashboard());
        }

        private void InitTheme()
        {
            panelLeft.BackColor = ColorTranslator.FromHtml("#2a3f54");
            panelTitle.BackColor = ColorTranslator.FromHtml("#2a3f54");
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            if (!String.IsNullOrEmpty(Program.Company.Name))
                labelCompanyName.Text = Program.Company.Name;
            labelUsername.Text = StringHelper.UcWords(Program.User.Username);
            roundButtonAvatar.BackgroundImage = ImageHelper.FromByteArray(Program.User.Avatar, Properties.Resources.empty);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            LoginForm.Show();
        }

        private void moveSidePanel(Control btn)
        {
            panelSide.Top = btn.Top;
            panelSide.Height = btn.Height;
        }

        private void AddControlsToPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            panelControls.Controls.Clear();
            panelControls.Controls.Add(c);
        }

        private void timerTime_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            labelTime.Text = dt.ToString("HH:MM:ss");
            labelDate.Text = dt.ToString("dddd, dd MMMM yyyy");
        }

        #region Drag Form
        private void panelHeader_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void panelHeader_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void panelHeader_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void buttonMaximize_Click(object sender, EventArgs e)
        {
            this.WindowState = this.WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal;
            this.buttonMaximize.Image = this.WindowState == FormWindowState.Normal ? Properties.Resources.maximize : Properties.Resources.restore;
            this.Top = (Screen.PrimaryScreen.Bounds.Height - Height) / 2;
            this.Left = (Screen.PrimaryScreen.Bounds.Width - Width) / 2;
        }

        private void panelHeader_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = this.WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal;
            this.buttonMaximize.Image = this.WindowState == FormWindowState.Normal ? Properties.Resources.maximize : Properties.Resources.restore;
            this.Top = (Screen.PrimaryScreen.Bounds.Height - Height) / 2;
            this.Left = (Screen.PrimaryScreen.Bounds.Width - Width) / 2;
        }
        #endregion

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCollapse_Click(object sender, EventArgs e)
        {
            timerCollapse.Start();
        }

        private void timerCollapse_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                panelLeft.Width = panelLeft.Width + 10;
                if (panelLeft.Width >= PanelWidth)
                {
                    timerCollapse.Stop();
                    isCollapsed = false;
                    this.Refresh();
                }
            }
            else
            {
                panelLeft.Width = panelLeft.Width - 10;
                if (panelLeft.Width <= 72)
                {
                    timerCollapse.Stop();
                    isCollapsed = true;
                    this.Refresh();
                }
            }
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            moveSidePanel((Control)sender);
            switch (((Control)sender).Name)
            {
                case "btnSales":
                    if (sales == null)
                        sales = new Sales();
                    AddControlsToPanel(sales);
                    break;
                case "btnPurchases":
                    if (purchases == null)
                        purchases = new Purchases();
                    AddControlsToPanel(purchases);
                    break;
                case "btnMaster":
                    if (master == null)
                        master = new MasterData();
                    AddControlsToPanel(master);
                    break;
                case "btnReports":
                    if (reports == null)
                        reports = new Reports();
                    AddControlsToPanel(reports);
                    break;
                case "btnSettings":
                    if (settings == null)
                    {
                        settings = new Settings();
                        settings.AppEvent = this;
                    }
                    AddControlsToPanel(settings);
                    break;
                default:
                    if (dashboard == null)
                        dashboard = new Dashboard();
                    AddControlsToPanel(dashboard);
                    break;
            }
        }

        private void userProfile_Click(object sender, EventArgs e)
        {
            Profile profile = new Profile();
            AddControlsToPanel(profile);
        }

        public void OnCompanyChanged(Contact company)
        {
            labelCompanyName.Text = company.Name;
            LoginForm.OnCompanyChanged(company);
        }

        public void OnUserChanged(User user)
        {
            labelUsername.Text = StringHelper.UcWords(user.Username);
            roundButtonAvatar.BackgroundImage = ImageHelper.FromByteArray(user.Avatar, Properties.Resources.empty);
        }
    }
}
