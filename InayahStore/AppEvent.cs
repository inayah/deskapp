﻿using Models.Domain;

namespace InayahStore
{
    public interface AppEvent
    {
        void OnUserChanged(User user);
        void OnCompanyChanged(Contact company);
    }
}
