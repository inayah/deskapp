﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace InayahStore
{
    public partial class CashierForm : Form
    {

        private bool mouseDown;
        private Point lastLocation;

        public LoginForm LoginForm { get; set; }

        public CashierForm()
        {
            InitializeComponent();
            SidePanel.Top = button1.Top;
            SidePanel.Height = button1.Height;
            firstCustomControl1.BringToFront();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            LoginForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button1.Height;
            SidePanel.Top = button1.Top;
            firstCustomControl1.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button2.Height;
            SidePanel.Top = button2.Top;
            mySecondCustmControl1.BringToFront();

            //Thanks for watching Friends...
            //Please dont forget to Subscribe... :) :) :) 
        }

        private void headerButtonClick(object sender, EventArgs e)
        {
            switch(((Control)sender).Name)
            {
                case "buttonLogout":
                    Close();
                    break;
                case "buttonSetup":
                    break;
            }
        }

        private void buttonMenu_Click(object sender, EventArgs e)
        {
            switch (((Control)sender).Name)
            {
                default:
                    SidePanel.Height = button1.Height;
                    SidePanel.Top = button1.Top;
                    firstCustomControl1.BringToFront();
                    break;
            }
        }

        #region Drag Form
        private void panelHeader_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void panelHeader_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void panelHeader_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void panelHeader_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = this.WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal;
            this.Top = (Screen.PrimaryScreen.Bounds.Height - Height) / 2;
            this.Left = (Screen.PrimaryScreen.Bounds.Width - Width) / 2;
        }
        #endregion
    }
}
