﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace InayahStore.Controls
{
    public enum TextBoxTypes { All, Numeric, Decimal, Letter, Code }

    public partial class CustomTextBox : TextBox
    {
        [DefaultValue(TextBoxTypes.All)]
        public TextBoxTypes TextBoxType { get; set; }

        public bool PasswordTextBox { get; set; }

        public bool NextFocusOnEnterKey { get; set; }

        [DefaultValue(0)]
        public int DecimalLength { get; set; }

        [Browsable(false)]
        public dynamic Value
        {
            get
            {
                if (TextBoxType == TextBoxTypes.Decimal)
                {
                    Decimal value = 0;
                    Decimal.TryParse(this.Text, out value);
                    return value;
                }
                else if (TextBoxType == TextBoxTypes.Numeric)
                {
                    int value = 0;
                    int.TryParse(this.Text, out value);
                    return value;
                }
                return Text;
            }
            set
            {
                Text = string.Format("{0:N0}", value);
            }
        }

        public bool IsEmpty
        {
            get { return String.IsNullOrEmpty(Text); }
        }

        public CustomTextBox()
        {
            TextChanged += onTextChanged;
            KeyPress += onKeyPress;
            KeyDown += onKeyDown;
        }

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && NextFocusOnEnterKey)
            {
                e.Handled = true;
                Parent.SelectNextControl((Control)sender, true, true, true, true);
            }
        }

        #region Event Handler
        private void onKeyPress(object sender, KeyPressEventArgs e)
        {
            switch (TextBoxType)
            {
                case TextBoxTypes.Decimal:
                    if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                        e.Handled = true;
                    if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                        e.Handled = true;
                    break;
                case TextBoxTypes.Numeric:
                    if ((sender as TextBox).SelectionStart == 0)
                        e.Handled = char.IsWhiteSpace(e.KeyChar) || char.IsLetter(e.KeyChar);
                    else
                        e.Handled = !char.IsDigit(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && !'\b'.Equals(e.KeyChar);
                    break;
                case TextBoxTypes.Letter:
                    if ((sender as TextBox).SelectionStart == 0)
                        e.Handled = char.IsWhiteSpace(e.KeyChar);
                    else
                        e.Handled = !char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && !'\b'.Equals(e.KeyChar);
                    break;
                case TextBoxTypes.Code:
                    if ((sender as TextBox).SelectionStart == 0)
                        e.Handled = char.IsWhiteSpace(e.KeyChar);
                    else
                        e.Handled = !char.IsLetter(e.KeyChar) && !char.IsDigit(e.KeyChar) && !'-'.Equals(e.KeyChar) && !'/'.Equals(e.KeyChar) && !'\b'.Equals(e.KeyChar);
                    break;
            }
        }

        private void onTextChanged(object sender, EventArgs e)
        {
            if (PasswordTextBox && !string.IsNullOrEmpty(Text))
            {
                UseSystemPasswordChar = true;
            }
            if (TextBoxType == TextBoxTypes.Decimal && !string.IsNullOrEmpty(Text))
            {
                Text = string.Format("{0:N0}", Convert.ToDecimal(Text));
                Select(Text.Length, 0);
            }
        }
        #endregion
    }
}
