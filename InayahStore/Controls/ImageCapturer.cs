﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WebEye.Controls.WinForms.WebCameraControl;

namespace InayahStore.Controls
{
    public partial class ImageCapturer : Form
    {
        public Image CurrentImage
        {
            get; set;
        }

        public ImageCapturer()
        {
            InitializeComponent();
        }

        private void ImageCapturer_Load(object sender, EventArgs e)
        {
            foreach (var camera in webCameraControl.GetVideoCaptureDevices())
            {
                comboBoxDevice.Items.Add(new CameraItem(camera));
            }

            if (comboBoxDevice.Items.Count > 0)
                comboBoxDevice.SelectedIndex = 0;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (webCameraControl.IsCapturing)
                webCameraControl.StopCapture();
        }

        internal Image ShowAsDialog(Form parent)
        {
            StartPosition = FormStartPosition.CenterParent;
            ShowInTaskbar = false;
            MaximizeBox = false;
            MinimizeBox = false;
            ShowDialog(parent);

            return CurrentImage;
        }

        private void comboBoxDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            var camera = (CameraItem)comboBoxDevice.SelectedItem;
            webCameraControl.StartCapture(camera.Id);
        }

        private void buttonCapture_Click(object sender, EventArgs e)
        {
            pictureBoxPreview.Image = webCameraControl.GetCurrentImage();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            var option = openFileDialog.ShowDialog();
            if (option == DialogResult.OK)
            {
                var file = openFileDialog.OpenFile();
                pictureBoxPreview.ImageLocation = openFileDialog.FileName;
            }
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            CurrentImage = pictureBoxPreview.Image;
            Close();
        }

        private class CameraItem
        {
            public CameraItem(WebCameraId id)
            {
                _id = id;
            }

            private readonly WebCameraId _id;
            public WebCameraId Id
            {
                get { return _id; }
            }

            public override string ToString()
            {
                return _id.Name;
            }
        }

    }
}
