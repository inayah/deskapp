﻿using InayahStore.Helper;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace InayahStore.Controls
{
    [Docking(DockingBehavior.Ask)]
    public partial class NavigationGrid : UserControl
    {
        public string DataSourceName { get; set; }
        public event Action<NavigationGridArgs> UpdateDataSource = delegate { };
        public event Action<DataGridViewRow> RowSelectionChanged = delegate { };
        public event Action<DataGridView, DataGridViewCellEventArgs> CellClick = delegate { };
        public event Action<DataGridView, DataGridViewCellMouseEventArgs> CellDoubleClick = delegate { };
        public event Action<DataGridView, DataGridViewCellFormattingEventArgs> CellFormatting = delegate { };
        public event EventHandler FilterButtonClick = delegate { };
        public event EventHandler AddButtonClick = delegate { };

        private ColumnSelector columnSelector;

        public NavigationGrid()
        {
            InitializeComponent();
            dataGrid.AutoGenerateColumns = false;
            dataGrid.DataSource = bindingSource.DataSource;
            columnSelector = new ColumnSelector(dataGrid);

            toolStripButtonAdd.Visible = ShowAddButton;
            toolStripButtonFilter.Visible = ShowFilterButton;
            toolStripSeparatorFilter.Visible = ShowFilterButton;
            comboPage.SelectedItem = 100;
        }

        public DataGridView DataGridView
        {
            get { return dataGrid; }
        }

        private bool showFilterButton;
        public bool ShowFilterButton
        {
            get { return showFilterButton; }
            set
            {
                showFilterButton = value;
                toolStripButtonFilter.Visible = showFilterButton;
            }
        }

        private bool showAddButton;
        public bool ShowAddButton
        {
            get { return showAddButton; }
            set
            {
                showAddButton = value;
                toolStripButtonAdd.Visible = showAddButton;
            }
        }

        private string addButtonText;
        public string AddButtonText
        {
            get { return addButtonText; }
            set
            {
                addButtonText = value;
                toolStripButtonAdd.Text = addButtonText;
            }
        }

        [Browsable(false)]
        public int RowCount { get; set; }

        [Browsable(false)]
        public int PageCount
        {
            get { return pageCount; }
            set { pageCount = value; }
        }

        [Browsable(false), ReadOnly(true)]
        public int CurrentPage
        {
            get { return currentPage; }
        }

        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        public DataGridViewColumn[] Columns
        {
            set
            {
                dataGrid.Columns.Clear();
                dataGrid.Columns.AddRange(value);
            }
        }

        [Browsable(false)]
        public DataGridViewRow SelectedRow { get; private set; }

        public void Reload()
        {
            toolStripTbSearch.Clear();
            UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize });
        }

        public void RefreshDataSource<T>(BindingList<T> bindingList, NavigationGridArgs e)
        {
            dataGrid.DataSource = bindingList;

            RowCount = e.RowCount;
            PageCount = e.PageCount;

            labelPageIndex.Text = String.Format("{0:#,##0}/{1:#,##0}", e.CurrentPage, e.PageCount);
            labelTotal.Text = "TOTAL : " + String.Format("{0:#,##0}", e.RowCount);

            int start = ((e.CurrentPage * e.PageSize) - e.PageSize) + 1;
            AutoNumberRows(dataGrid, start);
        }

        public void ClearSelection()
        {
            dataGrid.ClearSelection();
        }

        public void SelectRow(object value)
        {
            dataGrid.Focus();
            for (int index = 0; index < dataGrid.RowCount; index++)
            {
                DataGridViewRow row = dataGrid.Rows[index];
                if (row.Cells[0].Value.Equals(value))
                {
                    if (!row.Displayed)
                        dataGrid.FirstDisplayedScrollingRowIndex = index;
                    row.Selected = true;
                    break;
                }
            }
        }

        #region Datagrid Navigation

        private int pageSize = 100;
        private int pageCount = 1;
        private int currentPage = 1;
        private string orderColumn;
        private string orderMode;

        private void ButtonFirst_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage = 1;
                var sortedColumn = dataGrid.SortedColumn;
                var order = dataGrid.SortOrder == SortOrder.Ascending || dataGrid.SortOrder == SortOrder.None ? ListSortDirection.Ascending : ListSortDirection.Descending;

                UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
                if (sortedColumn != null)
                {
                    dataGrid.Sort(sortedColumn, order);
                }
            }
        }

        private void ButtonPrevious_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                var sortedColumn = dataGrid.SortedColumn;
                var order = dataGrid.SortOrder == SortOrder.Ascending || dataGrid.SortOrder == SortOrder.None ? ListSortDirection.Ascending : ListSortDirection.Descending;

                UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
                if (sortedColumn != null)
                {
                    dataGrid.Sort(sortedColumn, order);
                }
            }
        }

        private void ButtonNext_Click(object sender, EventArgs e)
        {
            if (currentPage < pageCount)
            {
                currentPage++;
                var sortedColumn = dataGrid.SortedColumn;
                var order = dataGrid.SortOrder == SortOrder.Ascending || dataGrid.SortOrder == SortOrder.None ? ListSortDirection.Ascending : ListSortDirection.Descending;

                UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
                if (sortedColumn != null)
                {
                    dataGrid.Sort(sortedColumn, order);
                }
            }
        }

        private void ButtonLast_Click(object sender, EventArgs e)
        {
            if (currentPage < pageCount)
            {
                currentPage = pageCount;
                var sortedColumn = dataGrid.SortedColumn;
                var order = dataGrid.SortOrder == SortOrder.Ascending || dataGrid.SortOrder == SortOrder.None ? ListSortDirection.Ascending : ListSortDirection.Descending;

                UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
                if (sortedColumn != null)
                {
                    dataGrid.Sort(sortedColumn, order);
                }
            }
        }

        private void ComboPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentPage = 1;
            pageSize = int.Parse(comboPage.Text);
            UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
        }

        private void ComboPage_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !'\b'.Equals(e.KeyChar);
        }

        private void ComboPage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                currentPage = 1;
                pageSize = int.Parse(comboPage.Text);
                UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
            }
        }

        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            AddButtonClick(sender, e);
        }
        private void toolStripButtonFilter_Click(object sender, EventArgs e)
        {
            FilterButtonClick(sender, e);
        }

        private void ToolStripButtonReload_Click(object sender, EventArgs e)
        {
            toolStripTbSearch.Clear();
            UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize });
        }

        private void ToolStripTbSearch_TextChanged(object sender, EventArgs e)
        {
            UpdateDataSource(new NavigationGridArgs { CurrentPage = 1, PageSize = pageSize, Filter = toolStripTbSearch.Text });
        }

        private void ToolStripButtonExport_Click(object sender, EventArgs e)
        {
            saveFileDialog.FileName = DataSourceName;
            var show = saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName) && show == DialogResult.OK)
            {
                var filePath = saveFileDialog.FileName;
                ExcelExporter.Export(dataGrid, filePath);
                //var exporter = new ExcelConvertor().Convert(dataGrid, saveFileDialog.FileName);

                if (MessageBox.Show("Open saved file?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    System.Diagnostics.Process.Start(filePath);
            }
        }

        private void ToolStripButtonPrint_Click(object sender, EventArgs e)
        {
            new PrintHelper().Print(dataGrid, DataSourceName);
            //GridViewPrinter printer = new GridViewPrinter();

            //PaperSize pageSize = new PaperSize();
            //pageSize.RawKind = (int)PaperKind.A4;

            //printer.printDocument.DefaultPageSettings.PaperSize = pageSize;
            //printer.PrintPreviewDataGridView(dataGrid);
            //printer.PrintDataGridView(dataGrid);
        }

        #endregion

        private void DataGrid_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                var type = dataGrid.Columns[e.ColumnIndex].CellType;
                if (type == typeof(DataGridViewImageCell) || typeof(DataGridViewImageCell) == type)
                    dataGrid.Cursor = Cursors.Hand;
                else
                    dataGrid.Cursor = Cursors.Default;
            }
        }

        private void DataGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            CellFormatting(dataGrid, e);
        }

        private void DataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            RowSelectionChanged(dataGrid.Rows[e.RowIndex]);
        }

        private void DataGrid_SelectionChanged(object sender, EventArgs e)
        {
            SelectedRow = dataGrid.SelectedRows.Count > 0 ? dataGrid.SelectedRows[0] : null;
            RowSelectionChanged(SelectedRow);
        }

        private void DataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            CellClick(dataGrid, e);
        }

        private void DataGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            CellDoubleClick(dataGrid, e);
        }
        
        private void DataGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewColumn newColumn = dataGrid.Columns[e.ColumnIndex];
            orderMode = newColumn.HeaderCell.SortGlyphDirection == SortOrder.Descending ? "DESC" : "ASC";
            orderColumn = newColumn.DataPropertyName;

            UpdateDataSource(new NavigationGridArgs { CurrentPage = currentPage, PageSize = pageSize, Filter = toolStripTbSearch.Text, OrderColumn = orderColumn, OrderMode = orderMode });
        }

        private void AutoNumberRows(DataGridView dataGridView, int start)
        {
            if (dataGridView != null)
            {
                for (int count = 0; (count <= (dataGridView.RowCount - 1)); count++)
                {
                    dataGridView.Rows[count].HeaderCell.Value = string.Format("{0:#,##0}", count + start);
                }
            }
        }

        public class NavigationGridArgs : EventArgs
        {
            private int pageSize = 50;
            private int currentPage = 1;

            public int PageSize { get { return pageSize; } set { pageSize = value; } }
            public int CurrentPage { get { return currentPage; } set { currentPage = value; } }
            public int PageCount { get; set; }
            public int RowCount { get; set; }
            public string Filter { get; set; }
            public string OrderColumn { get; set; }
            public string OrderMode { get; set; }
        }
    }
}
