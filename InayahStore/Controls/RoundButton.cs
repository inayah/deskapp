﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace InayahStore.Controls
{
    public partial class RoundButton : Button
    {
        public RoundButton()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var graphicsPath = new GraphicsPath();
            graphicsPath.AddEllipse(0, 0, ClientSize.Width, ClientSize.Height);
            this.Region = new Region(graphicsPath);
            base.OnPaint(e);
        }
    }
}
