﻿using InayahStore.Controls;

namespace InayahStore.Forms
{
    partial class ProductInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBoxTax = new InayahStore.Controls.CustomTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxUom = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPrice = new InayahStore.Controls.CustomTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxMarkup = new InayahStore.Controls.CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCost = new InayahStore.Controls.CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxCategories = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCode = new InayahStore.Controls.CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dragControl1 = new InayahStore.Controls.DragControl();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(878, 373);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.textBoxTax);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.comboBoxUom);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.textBoxPrice);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.textBoxMarkup);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.textBoxCost);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.comboBoxCategories);
            this.panel5.Controls.Add(this.textBoxName);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.textBoxCode);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.pictureBoxImage);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Location = new System.Drawing.Point(20, 43);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(841, 264);
            this.panel5.TabIndex = 32;
            // 
            // textBoxTax
            // 
            this.textBoxTax.Location = new System.Drawing.Point(320, 212);
            this.textBoxTax.Name = "textBoxTax";
            this.textBoxTax.NextFocusOnEnterKey = true;
            this.textBoxTax.PasswordTextBox = false;
            this.textBoxTax.Size = new System.Drawing.Size(261, 27);
            this.textBoxTax.TabIndex = 7;
            this.textBoxTax.Text = "0";
            this.textBoxTax.TextBoxType = InayahStore.Controls.TextBoxTypes.Decimal;
            this.textBoxTax.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(316, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 19);
            this.label9.TabIndex = 40;
            this.label9.Text = "Pajak (%)";
            // 
            // comboBoxUom
            // 
            this.comboBoxUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUom.FormattingEnabled = true;
            this.comboBoxUom.Items.AddRange(new object[] {
            "Admin",
            "Employee"});
            this.comboBoxUom.Location = new System.Drawing.Point(16, 210);
            this.comboBoxUom.Name = "comboBoxUom";
            this.comboBoxUom.Size = new System.Drawing.Size(261, 29);
            this.comboBoxUom.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 188);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 19);
            this.label8.TabIndex = 38;
            this.label8.Text = "Satuan";
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(320, 152);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.NextFocusOnEnterKey = true;
            this.textBoxPrice.PasswordTextBox = false;
            this.textBoxPrice.Size = new System.Drawing.Size(261, 27);
            this.textBoxPrice.TabIndex = 6;
            this.textBoxPrice.Text = "0";
            this.textBoxPrice.TextBoxType = InayahStore.Controls.TextBoxTypes.Decimal;
            this.textBoxPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(316, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 19);
            this.label7.TabIndex = 36;
            this.label7.Text = "Harga Jual";
            // 
            // textBoxMarkup
            // 
            this.textBoxMarkup.Location = new System.Drawing.Point(320, 94);
            this.textBoxMarkup.Name = "textBoxMarkup";
            this.textBoxMarkup.NextFocusOnEnterKey = true;
            this.textBoxMarkup.PasswordTextBox = false;
            this.textBoxMarkup.Size = new System.Drawing.Size(261, 27);
            this.textBoxMarkup.TabIndex = 5;
            this.textBoxMarkup.Text = "0";
            this.textBoxMarkup.TextBoxType = InayahStore.Controls.TextBoxTypes.Decimal;
            this.textBoxMarkup.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(316, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 19);
            this.label6.TabIndex = 34;
            this.label6.Text = "Markup (%)";
            // 
            // textBoxCost
            // 
            this.textBoxCost.DecimalLength = 2;
            this.textBoxCost.Location = new System.Drawing.Point(320, 38);
            this.textBoxCost.Name = "textBoxCost";
            this.textBoxCost.NextFocusOnEnterKey = true;
            this.textBoxCost.PasswordTextBox = false;
            this.textBoxCost.Size = new System.Drawing.Size(261, 27);
            this.textBoxCost.TabIndex = 4;
            this.textBoxCost.Text = "0";
            this.textBoxCost.TextBoxType = InayahStore.Controls.TextBoxTypes.Decimal;
            this.textBoxCost.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(316, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 19);
            this.label4.TabIndex = 32;
            this.label4.Text = "Harga Beli";
            // 
            // comboBoxCategories
            // 
            this.comboBoxCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCategories.FormattingEnabled = true;
            this.comboBoxCategories.Items.AddRange(new object[] {
            "Admin",
            "Employee"});
            this.comboBoxCategories.Location = new System.Drawing.Point(16, 150);
            this.comboBoxCategories.Name = "comboBoxCategories";
            this.comboBoxCategories.Size = new System.Drawing.Size(261, 29);
            this.comboBoxCategories.TabIndex = 2;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(16, 92);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(261, 27);
            this.textBoxName.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 19);
            this.label5.TabIndex = 25;
            this.label5.Text = "Kategori";
            // 
            // textBoxCode
            // 
            this.textBoxCode.Location = new System.Drawing.Point(16, 36);
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.NextFocusOnEnterKey = true;
            this.textBoxCode.PasswordTextBox = false;
            this.textBoxCode.Size = new System.Drawing.Size(261, 27);
            this.textBoxCode.TabIndex = 0;
            this.textBoxCode.TextBoxType = InayahStore.Controls.TextBoxTypes.Code;
            this.textBoxCode.Value = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 19);
            this.label3.TabIndex = 26;
            this.label3.Text = "Nama";
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxImage.Image = global::InayahStore.Properties.Resources.empty;
            this.pictureBoxImage.Location = new System.Drawing.Point(604, 12);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(222, 227);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImage.TabIndex = 28;
            this.pictureBoxImage.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 19);
            this.label2.TabIndex = 27;
            this.label2.Text = "Kode";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.DarkGray;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(625, 322);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(97, 38);
            this.buttonCancel.TabIndex = 30;
            this.buttonCancel.Text = "&Tutup";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.BackColor = System.Drawing.Color.SeaGreen;
            this.buttonSave.FlatAppearance.BorderSize = 0;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(728, 322);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(133, 38);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "&Simpan";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.SystemColors.Control;
            this.buttonClose.FlatAppearance.BorderSize = 0;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClose.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(836, 3);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(39, 34);
            this.buttonClose.TabIndex = 30;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 23);
            this.label1.TabIndex = 28;
            this.label1.Text = "Input Barang";
            // 
            // dragControl1
            // 
            this.dragControl1.SelectControl = this.panel1;
            // 
            // ProductInput
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.ClientSize = new System.Drawing.Size(890, 385);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductInput";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBoxCategories;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label5;
        private CustomTextBox textBoxCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label label1;
        private DragControl dragControl1;
        private CustomTextBox textBoxPrice;
        private System.Windows.Forms.Label label7;
        private CustomTextBox textBoxMarkup;
        private System.Windows.Forms.Label label6;
        private CustomTextBox textBoxCost;
        private System.Windows.Forms.Label label4;
        private CustomTextBox textBoxTax;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxUom;
        private System.Windows.Forms.Label label8;
    }
}