﻿using DataLayer.Repository;
using DataLayer.UnitOfWorks;
using InayahStore.Helper;
using Models.Domain;
using NHibernate.Criterion;
using Services;
using System;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore.Forms
{
    public partial class ProductInput : Form
    {
        public Product Product
        {
            get { return _product; }
            set { _product = value; IsUpdate = true; }
        }

        private Product _product;
        private bool IsUpdate;

        public ProductInput()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            if (IsUpdate)
            {
                textBoxCode.Text = Product.Code;
                textBoxName.Text = Product.Name;
                textBoxCost.Value = Product.Cost;
                textBoxMarkup.Value = Product.Markup;
                textBoxPrice.Value = Product.Price;
                textBoxTax.Value = Product.Tax;
            }
            else
            {
                Product = new Product();
                string Code = UnitOfWork.CurrentSession.QueryOver<Product>().Select(Projections.Max<Product>(x => x.Code)).SingleOrDefault<string>();
                Product.Code = StringHelper.NextCode("BR-", Code, 4);

                textBoxCode.Text = Product.Code;
                textBoxCode.Select();
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Hide();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            this.Hide();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var productRepository = new Repository<Product>();
            EntityService<Product> service = new EntityService<Product>(productRepository);
            service.OnError += Service_OnError;
            service.Saved += Service_Saved;

            Product.Code = textBoxCode.Text;
            Product.Name = textBoxName.Text;
            Product.Cost = textBoxCost.Value;
            Product.Markup = textBoxMarkup.Value;
            Product.Price = textBoxPrice.Value;
            Product.Tax = textBoxTax.Value;

            service.Save(Product);
        }

        private void Service_OnError(object sender, EntityEventArgs<Product> e)
        {
            MessageBox.Show(this, e.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Service_Saved(object sender, EntityEventArgs<Product> e)
        {
            UnitOfWork.Current.TransactionalFlush();
            MessageBox.Show(this, "Data Barang telah disimpan", "Information", MessageBoxButtons.OK);

            DialogResult = DialogResult.OK;
            this.Dispose();
        }
    }
}
