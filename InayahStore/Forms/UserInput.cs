﻿using DataLayer.Repository;
using DataLayer.UnitOfWorks;
using Models.Domain;
using Services;
using System;
using System.Windows.Forms;

namespace InayahStore.Forms
{
    public partial class UserInput : Form
    {
        public AppEvent AppEvent;
        public User User
        {
            get { return _user; }
            set { _user = value; IsUpdate = true; }
        }

        private User _user;
        private bool IsUpdate;

        public UserInput()
        {
            InitializeComponent();

            comboBoxRoles.DataSource = Enum.GetValues(typeof(Role));
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            if (!IsUpdate) User = new User();

            textBoxUsername.Text = User.Username;
            comboBoxRoles.SelectedItem = User.Role;
            checkBoxActive.Checked = User.IsActive;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(textBoxUsername.Text))
            {
                errorProvider.SetError(textBoxUsername, "Username tidak boleh kosong");
                return;
            }

            if (!IsUpdate && String.IsNullOrEmpty(textBoxPassword.Text))
            {
                errorProvider.SetError(textBoxUsername, "Password tidak boleh kosong");
                return;
            }

            errorProvider.Clear();

            User.Username = textBoxUsername.Text;
            User.Role = (Role)comboBoxRoles.SelectedItem;
            User.Status = checkBoxActive.Checked ? UserStatus.ENABLED : UserStatus.DISABLED;
            if (!String.IsNullOrEmpty(textBoxPassword.Text))
            {
                User.Password = BCrypt.Net.BCrypt.HashPassword(textBoxPassword.Text);
            }

            var userRepository = new Repository<User>();
            EntityService<User> service = new EntityService<User>(userRepository);
            service.Saved += onUser_Saved;
            service.Save(User);
        }

        private void onUser_Saved(object sender, EntityEventArgs<User> e)
        {
            UnitOfWork.Current.TransactionalFlush();
            if (AppEvent != null) AppEvent.OnUserChanged(e.Entity);

            MessageBox.Show("Perubahan berhasil disimpan", "Success");
            DialogResult = DialogResult.OK;
            this.Dispose();
        }
    }
}
