using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace InayahStore.Helper
{
    /// <summary>
    /// Summary description for CSVConvertor.
    /// </summary>
    public class CSVConvertor
    {
        /// <summary>
        /// To generate CSV file.
        /// </summary>
        /// <param name="dataGrid"></param>
        /// <param name="directoryPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string Convert(DataGridView dataGrid, string fullpath)
        {
            StreamWriter SW;
            SW = File.CreateText(fullpath);

            StringBuilder oStringBuilder = new StringBuilder();

            /*******************************************************************
			 * Start, Creating column header
			 * *****************************************************************/

            foreach (DataGridViewColumn oDataColumn in dataGrid.Columns)
            {
                oStringBuilder.Append(oDataColumn.HeaderText + ",");
            }

            SW.WriteLine(oStringBuilder.ToString().Substring(0, oStringBuilder.ToString().Length - 1));
            oStringBuilder.Length = 0;

            /*******************************************************************
			 * End, Creating column header
			 * *****************************************************************/

            /*******************************************************************
			 * Start, Creating rows
			 * *****************************************************************/

            foreach (DataGridViewRow oDataRow in dataGrid.Rows)
            {
                foreach (DataGridViewColumn oDataColumn in dataGrid.Columns)
                {
                    oStringBuilder.Append(oDataRow.Cells[oDataColumn.Name] + ",");
                }
                SW.WriteLine(oStringBuilder.ToString().Substring(0, oStringBuilder.ToString().Length - 1));
                oStringBuilder.Length = 0;

            }


            /*******************************************************************
			 * End, Creating rows
			 * *****************************************************************/

            SW.Close();

            return fullpath;
        }
    }
}
