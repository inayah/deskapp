using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InayahStore.Helper
{
    /// <summary>
    /// To generate excel file.
    /// </summary>
    public class ExcelConvertor
    {


        /// <summary>
        /// To generate excel file.
        /// </summary>
        /// <param name="dataGrid"></param>
        /// <param name="directoryPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string Convert(DataGridView dataGrid, string fullpath)
        {
            StreamWriter SW;
            SW = File.CreateText(fullpath);


            StringBuilder oStringBuilder = new StringBuilder();

            /********************************************************
			 * Start, check for border width
			 * ******************************************************/
            int borderWidth = 0;

            if (_ShowExcelTableBorder)
            {
                borderWidth = 1;
            }
            /********************************************************
			 * End, Check for border width
			 * ******************************************************/

            /********************************************************
			 * Start, Check for bold heading
			 * ******************************************************/
            string boldTagStart = "";
            string boldTagEnd = "";
            if (_ExcelHeaderBold)
            {
                boldTagStart = "<B>";
                boldTagEnd = "</B>";
            }

            /********************************************************
			 * End,Check for bold heading
			 * ******************************************************/

            oStringBuilder.Append("<Table border=" + borderWidth + ">");

            /*******************************************************************
			 * Start, Creating table header
			 * *****************************************************************/

            oStringBuilder.Append("<TR>");

            IEnumerable<DataGridViewColumn> columns = (from DataGridViewColumn col in dataGrid.Columns where col.Visible == true && col.CellType == typeof(DataGridViewTextBoxCell) select col);
            foreach (DataGridViewColumn oDataColumn in columns)
            {
                oStringBuilder.Append("<TD>" + boldTagStart + oDataColumn.HeaderText + boldTagEnd + "</TD>");
            }

            oStringBuilder.Append("</TR>");

            /*******************************************************************
			 * End, Creating table header
			 * *****************************************************************/

            /*******************************************************************
			 * Start, Creating rows
			 * *****************************************************************/

            foreach (DataGridViewRow oDataRow in dataGrid.Rows)
            {
                oStringBuilder.Append("<TR>");

                foreach (DataGridViewColumn oDataColumn in dataGrid.Columns)
                {
                    if (oDataRow.Cells[oDataColumn.Index].Value is long)
                    {
                        oStringBuilder.Append("<TD align=right>" + oDataRow.Cells[oDataColumn.Name].Value + "</TD>");
                    }
                    else
                    {
                        oStringBuilder.Append("<TD>" + oDataRow.Cells[oDataColumn.Index].Value + "</TD>");
                    }


                }

                oStringBuilder.Append("</TR>");
            }


            /*******************************************************************
			 * End, Creating rows
			 * *****************************************************************/



            oStringBuilder.Append("</Table>");

            SW.WriteLine(oStringBuilder.ToString());
            SW.Close();

            return fullpath;
        }

        private bool _ShowExcelTableBorder = false;

        /// <summary>
        /// To show or hide the excel table border
        /// </summary>
        public bool ShowExcelTableBorder
        {
            get
            {
                return _ShowExcelTableBorder;
            }
            set
            {
                _ShowExcelTableBorder = value;
            }
        }

        private bool _ExcelHeaderBold = true;


        /// <summary>
        /// To make header bold or normal
        /// </summary>
        public bool ExcelHeaderBold
        {
            get
            {
                return _ExcelHeaderBold;
            }
            set
            {
                ExcelHeaderBold = value;
            }
        }
    }
}
