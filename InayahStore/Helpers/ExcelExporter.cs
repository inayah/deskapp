using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore.Helper
{
    public class ExcelExporter
    {
        public static void Export(DataGridView dataGrid, string fullPath)
        {
            var stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
            Export(dataGrid, stream, "xlsx");
        }

        public static void Export(DataGridView dataGrid, Stream fullPath, string extension)
        {
            IWorkbook workbook;
            ICellStyle cellStyle;

            if (extension == "xlsx")
            {
                workbook = new XSSFWorkbook();
                cellStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            }
            else if (extension == "xls")
            {
                workbook = new HSSFWorkbook();
                cellStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            }
            else
            {
                throw new Exception("This format is not supported");
            }

            cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            cellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

            ISheet sheet1 = workbook.CreateSheet();
            IRow row1 = sheet1.CreateRow(0);
            
            List<DataGridViewColumn> columns = (from DataGridViewColumn col in dataGrid.Columns where col.Visible == true select col).ToList();
            for (int j = 0; j < columns.Count(); j++)
            {
                ICell cell = row1.CreateCell(j);
                string columnName = columns[j].HeaderText;
                cell.SetCellValue(columnName);
                cell.CellStyle = cellStyle;
            }

            for (int i = 0; i < dataGrid.Rows.Count; i++)
            {
                IRow row = sheet1.CreateRow(i + 1);
                DataGridViewRow gridRow = dataGrid.Rows[i];
                List<DataGridViewCell> cells = (from DataGridViewCell col in gridRow.Cells where col.Visible == true select col).ToList();
                for (int j = 0; j < columns.Count(); j++)
                {
                    ICell cell = row.CreateCell(j);
                    cell.SetCellValue(Convert.ToString(cells[j].Value));
                    cell.CellStyle = cellStyle;
                }
            }

            workbook.Write(fullPath);
        }
    }
}
