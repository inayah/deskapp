﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;

namespace InayahStore.Helper
{
    class ImageHelper
    {
        private static int BUFFER_SIZE = 64 * 1024; //64kB

        public static byte[] ToByteArray(string ImageFile)
        {
            FileStream stream = new FileStream(ImageFile, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);

            byte[] photo = reader.ReadBytes((int)stream.Length);
            return Compress(photo);
        }

        public static byte[] ToByteArray(Image image)
        {
            byte[] imageArray;

            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            imageArray = new byte[ms.Length];
            ms.Seek(0, SeekOrigin.Begin);
            ms.Read(imageArray, 0, (int)ms.Length);
            return Compress(ms.GetBuffer());
        }

        public static Image FromByteArray(byte[] byteArrayIn, Image defaultImage)
        {
            return byteArrayIn == null ? defaultImage : FromByteArray(byteArrayIn);
        }

        public static Image FromByteArray(byte[] byteArrayIn)
        {
            var decompressed = Decompress(byteArrayIn);
            MemoryStream ms = new MemoryStream(decompressed);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static Icon ToImageIcon(byte[] byteArrayIn)
        {
            var decompressed = Decompress(byteArrayIn);
            MemoryStream ms = new MemoryStream(decompressed);
            Bitmap bitmap = new Bitmap(ms);
            bitmap.SetResolution(32, 32);

            return Icon.FromHandle(bitmap.GetHicon());
        }

        public static Icon ToImageIcon(Bitmap bitmap)
        {
            bitmap.SetResolution(32, 32);
            return Icon.FromHandle(bitmap.GetHicon());
        }

        public static byte[] Compress(byte[] inputData)
        {
            using (var compressIntoMs = new MemoryStream())
            {
                using (var gzs = new BufferedStream(new GZipStream(compressIntoMs, CompressionMode.Compress), BUFFER_SIZE))
                {
                    gzs.Write(inputData, 0, inputData.Length);
                }
                return compressIntoMs.ToArray();
            }
        }

        public static byte[] Decompress(byte[] inputData)
        {
            if (inputData == null) return null;

            using (var compressedMs = new MemoryStream(inputData))
            {
                using (var decompressedMs = new MemoryStream())
                {
                    using (var gzs = new BufferedStream(new GZipStream(compressedMs, CompressionMode.Decompress), BUFFER_SIZE))
                    {
                        gzs.CopyTo(decompressedMs);
                    }
                    return decompressedMs.ToArray();
                }
            }
        }
    }
}
