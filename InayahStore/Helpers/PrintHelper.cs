﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Drawing;
using System.Collections;

namespace InayahStore.Helper
{
    class PrintHelper
    {
        private string header;
        private DataGridView dataGrid;
        private StringFormat strFormat;

        ArrayList arrColumnLefts = new ArrayList();//Used to save left coordinates of columns
        ArrayList arrColumnWidths = new ArrayList();//Used to save column widths
        int iCellHeight = 0; //Used to get/set the datagridview cell height
        int iTotalWidth = 0; //
        int iRow = 0;//Used as counter
        bool bFirstPage = false; //Used to check whether we are printing first page
        bool bNewPage = false;// Used to check whether we are printing a new page
        int iHeaderHeight = 0; //Used for the header height

        public void Print(DataGridView dataGrid, string header)
        {
            this.header = header;
            this.dataGrid = dataGrid;
            PrintDocument doc = new PrintDocument();

            //PaperSize pageSize = new PaperSize();
            //pageSize.RawKind = (int)PaperKind.A4;

            //doc.DefaultPageSettings.Landscape = true;
            //doc.DefaultPageSettings.PaperSize = pageSize;
            //doc.DefaultPageSettings.Margins = new Margins(5, 0, 0, 0);

            doc.BeginPrint += new PrintEventHandler(doc_BeginPrint);
            doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);

            DialogResult answer = MessageBox.Show("Open Print Preview?", "Question", MessageBoxButtons.YesNo);
            if (answer == System.Windows.Forms.DialogResult.No)
            {
                PageSetupDialog page = new PageSetupDialog();
                page.Document = doc;
                DialogResult r = page.ShowDialog();
                if (r == DialogResult.OK)
                {
                    doc.Print();
                }
            }
            else
            {
                PrintPreviewDialog preview = new PrintPreviewDialog();
                preview.Document = doc;
                preview.ShowDialog();
            }
        }

        void doc_BeginPrint(object sender, PrintEventArgs e)
        {
            try
            {
                strFormat = new StringFormat();
                strFormat.Alignment = StringAlignment.Near;
                strFormat.LineAlignment = StringAlignment.Center;
                strFormat.Trimming = StringTrimming.EllipsisCharacter;

                arrColumnLefts.Clear();
                arrColumnWidths.Clear();
                iCellHeight = 0;
                bFirstPage = true;
                bNewPage = true;

                // Calculating Total Widths
                iTotalWidth = 0;

                IEnumerable<DataGridViewColumn> columns = (from DataGridViewColumn col in dataGrid.Columns where col.Visible == true select col);
                foreach (DataGridViewColumn dgvGridCol in columns)
                {
                    iTotalWidth += dgvGridCol.Width;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                //Set the left margin
                int iLeftMargin = e.MarginBounds.Left;
                //Set the top margin
                int iTopMargin = e.MarginBounds.Top;
                //Whether more pages have to print or not
                bool bMorePagesToPrint = false;
                int iTmpWidth = 0;

                IEnumerable<DataGridViewColumn> columns = (from DataGridViewColumn col in dataGrid.Columns where col.Visible == true select col);

                //For the first page to print set the cell width and header height
                if (bFirstPage)
                {
                    foreach (DataGridViewColumn GridCol in columns)
                    {
                        iTmpWidth = (int)(Math.Floor((double)((double)GridCol.Width /
                            (double)iTotalWidth * (double)iTotalWidth *
                            ((double)e.MarginBounds.Width / (double)iTotalWidth))));

                        iHeaderHeight = (int)(e.Graphics.MeasureString(GridCol.HeaderText,
                            GridCol.InheritedStyle.Font, iTmpWidth).Height) + 11;

                        // Save width and height of headers
                        arrColumnLefts.Add(iLeftMargin);
                        arrColumnWidths.Add(iTmpWidth);
                        iLeftMargin += iTmpWidth;
                    }
                }
                //Loop till all the grid rows not get printed
                while (iRow <= dataGrid.Rows.Count - 1)
                {
                    DataGridViewRow GridRow = dataGrid.Rows[iRow];
                    //Set the cell height
                    iCellHeight = GridRow.Height + 5;
                    int iCount = 0;
                    //Check whether the current page settings allows more rows to print
                    if (iTopMargin + iCellHeight >= e.MarginBounds.Height + e.MarginBounds.Top)
                    {
                        bNewPage = true;
                        bFirstPage = false;
                        bMorePagesToPrint = true;
                        break;
                    }
                    else
                    {
                        if (bNewPage)
                        {
                            //Draw Header
                            e.Graphics.DrawString(header,
                                new Font(dataGrid.Font, FontStyle.Bold),
                                Brushes.Black, e.MarginBounds.Left,
                                e.MarginBounds.Top - e.Graphics.MeasureString(header,
                                new Font(dataGrid.Font, FontStyle.Bold),
                                e.MarginBounds.Width).Height - 13);

                            String strDate = DateTime.Now.ToLongDateString() + " " +
                                DateTime.Now.ToShortTimeString();
                            //Draw Date
                            e.Graphics.DrawString(strDate,
                                new Font(dataGrid.Font, FontStyle.Bold), Brushes.Black,
                                e.MarginBounds.Left +
                                (e.MarginBounds.Width - e.Graphics.MeasureString(strDate,
                                new Font(dataGrid.Font, FontStyle.Bold),
                                e.MarginBounds.Width).Width),
                                e.MarginBounds.Top - e.Graphics.MeasureString(header,
                                new Font(new Font(dataGrid.Font, FontStyle.Bold),
                                FontStyle.Bold), e.MarginBounds.Width).Height - 13);

                            //Draw Columns                 
                            iTopMargin = e.MarginBounds.Top;
                            foreach (DataGridViewColumn GridCol in columns)
                            {
                                e.Graphics.FillRectangle(new SolidBrush(Color.LightGray),
                                    new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                                    (int)arrColumnWidths[iCount], iHeaderHeight));

                                e.Graphics.DrawRectangle(Pens.Black,
                                    new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                                    (int)arrColumnWidths[iCount], iHeaderHeight));

                                e.Graphics.DrawString(GridCol.HeaderText,
                                    GridCol.InheritedStyle.Font,
                                    new SolidBrush(GridCol.InheritedStyle.ForeColor),
                                    new RectangleF((int)arrColumnLefts[iCount], iTopMargin,
                                    (int)arrColumnWidths[iCount], iHeaderHeight), strFormat);
                                iCount++;
                            }
                            bNewPage = false;
                            iTopMargin += iHeaderHeight;
                        }
                        iCount = 0;
                        //Draw Columns Contents                

                        IEnumerable<DataGridViewCell> Cells = (from DataGridViewCell col in GridRow.Cells where col.Visible == true select col);
                        foreach (DataGridViewCell Cel in Cells)
                        {
                            if (Cel.Value != null)
                            {
                                string value = Cel.Value is DateTime ? string.Format("{0:dd-MMM-yyyy}", Cel.Value) : Convert.ToString(Cel.Value);
                                RectangleF recF = new RectangleF((int)arrColumnLefts[iCount], iTopMargin, (int)arrColumnWidths[iCount], iCellHeight);
                                SolidBrush brush = new SolidBrush(Cel.InheritedStyle.ForeColor);
                                e.Graphics.DrawString(value, Cel.InheritedStyle.Font, brush, recF, strFormat);
                            }
                            //Drawing Cells Borders 
                            e.Graphics.DrawRectangle(Pens.Black, new Rectangle((int)arrColumnLefts[iCount], iTopMargin, (int)arrColumnWidths[iCount], iCellHeight));
                            iCount++;
                        }
                    }
                    iRow++;
                    iTopMargin += iCellHeight;
                }
                //If more lines exist, print another page.
                if (bMorePagesToPrint)
                    e.HasMorePages = true;
                else
                    e.HasMorePages = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK);
            }
        }

        static readonly PrintHelper _instance = new PrintHelper();
        public static PrintHelper Instance
        {
            get { return _instance; }
        }

    }
}
