﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace InayahStore.Helper
{
    static class StringHelper
    {
        public static String NextCode(String prefix, String lastCode = "", int length = 4)
        {
            lastCode = String.IsNullOrEmpty(lastCode) ? String.Format("{0}{1}", prefix, new String('0', length)) : lastCode;
            lastCode = String.Format("{0}{1}", new String('0', length), (Convert.ToInt32(lastCode.Substring(lastCode.Length - length, length)) + 1));
            return String.Format("{0}{1}", prefix, lastCode.Substring(lastCode.Length - length, length));
        }

        public static string UcWords(this String s)
        {
            return s.ToCharArray().Aggregate(String.Empty,
              (working, next) =>
                working.Length == 0 && next != ' ' ? next.ToString().ToUpper() : (
                  working.EndsWith(" ") ? working + next.ToString().ToUpper() :
                    working + next.ToString()
              )
            );
        }

        public static string BreakCamelCase(this String s)
        {
            string strRegex = @"(?<=[a-z])([A-Z])|(?<=[A-Z])([A-Z][a-z])";
            Regex regex = new Regex(strRegex, RegexOptions.None);
            return regex.Replace(s, " $1$2");
        }

        public static string GetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            return GetHexString(sec.ComputeHash(bt));
        }

        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte b = bt[i];
                int n, n1, n2;
                n = (int)b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char)(n2 - 10 + (int)'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char)(n1 - 10 + (int)'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
            }
            return s;
        }
    }
}
