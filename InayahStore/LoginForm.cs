﻿using DataLayer.Repository;
using InayahStore.Helper;
using InayahStore.Properties;
using Models.Domain;
using Services;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore
{
    public partial class LoginForm : Form, AppEvent
    {
        private bool mouseDown;
        private Point lastLocation;

        public LoginForm()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            pictureBoxLogoLarge.Image = ImageHelper.FromByteArray(Program.Company.Image, Resources.logo_circle);
            if (!String.IsNullOrEmpty(Program.Company.Name))
                labelCompanyName.Text = Program.Company.Name;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            textBoxUsername.Text = Settings.Default.Remember ? Settings.Default.Username : textBoxUsername.Text;
            textBoxPassword.Text = Settings.Default.Remember ? Settings.Default.Password : textBoxPassword.Text;
            checkBoxRemember.Checked = Settings.Default.Remember;
            textBoxUsername.Select();
        }

        private User ValidateUser()
        {
            errorProvider.Clear();
            if (string.IsNullOrEmpty(textBoxUsername.Text))
            {
                errorProvider.SetError(textBoxUsername, "Username is empty");
                textBoxUsername.Focus();
                return null;
            }
            if (string.IsNullOrEmpty(textBoxPassword.Text))
            {
                errorProvider.SetError(textBoxPassword, "Password is empty");
                textBoxPassword.Focus();
                return null;
            }

            var _UserRepository = new Repository<User>();
            EntityService<User> service = new EntityService<User>(_UserRepository);
            var User = service.FindByProperty("Username", textBoxUsername.Text).FirstOrDefault();

            if (User != null)
            {
                var verified = BCrypt.Net.BCrypt.Verify(textBoxPassword.Text, User.Password);
                if (!verified)
                {
                    errorProvider.SetError(textBoxPassword, "Invalid Password");
                    textBoxPassword.Focus();
                    return null;
                }
            }
            else
            {
                errorProvider.SetError(textBoxUsername, "Unknown Username");
                textBoxUsername.Focus();
                return null;
            }
            return User;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            var User = ValidateUser();
            if (User != null)
            {
                Settings.Default.Username = checkBoxRemember.Checked ? textBoxUsername.Text : string.Empty;
                Settings.Default.Password = checkBoxRemember.Checked ? textBoxPassword.Text : string.Empty;
                Settings.Default.Remember = checkBoxRemember.Checked;
                Settings.Default.Save();

                if (User.Role == Role.ADMIN)
                {
                    var form = new AdminForm()
                    {
                        LoginForm = this
                    };
                    form.Show();
                }
                else
                {
                    var form = new CashierForm()
                    {
                        LoginForm = this
                    };
                    form.Show();
                }

                Program.User = User;
                this.Hide();
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void panelHeader_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void panelHeader_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void panelHeader_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        public void OnCompanyChanged(Contact company)
        {
            labelCompanyName.Text = company.Name;
            pictureBoxLogoSmall.Image = ImageHelper.FromByteArray(company.Image);
            pictureBoxLogoLarge.Image = ImageHelper.FromByteArray(company.Image);
        }

        public void OnUserChanged(User user)
        {
            throw new NotImplementedException();
        }
    }
}
