﻿using CrashReporterDotNET;
using DataLayer.Repository;
using DataLayer.UnitOfWorks;
using DeviceId;
using DeviceId.Encoders;
using DeviceId.Formatters;
using InayahStore.Helper;
using Models.Domain;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Services;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace InayahStore
{
    static class Program
    {

        private static string DbFile = "core.db";

        public static User User { get; set; }
        public static Contact Company { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += delegate (object sender, ThreadExceptionEventArgs e)
            {
                //if (!Debugger.IsAttached)
                //    ReportCrash(e.Exception);
                //else
                //    ThrowException(e.Exception);
            };

            AppDomain.CurrentDomain.UnhandledException += delegate (object sender, UnhandledExceptionEventArgs e)
            {
                //if (!Debugger.IsAttached)
                //    ReportCrash((Exception)e.ExceptionObject);
                //else
                //    ThrowException((Exception)e.ExceptionObject);
                //System.Environment.Exit(0);
            };

            using (UnitOfWork.Start(DbFile))
            {
                BuildSchema(UnitOfWork.Configuration);

                Company = UnitOfWork.CurrentSession.Query<Contact>().Where(c => c.ContactType == ContactType.COMPANY).FirstOrDefault();
                Company = Company != null ? Company : new Contact();

                var _UserRepository = new Repository<User>();
                EntityService<User> service = new EntityService<User>(_UserRepository);
                service.Saved += Service_Saved;

                var admin = service.FindByProperty("Username", "admin").FirstOrDefault();
                if (admin == null)
                {
                    var tableAcc = new User
                    {
                        Username = "admin",
                        Password = BCrypt.Net.BCrypt.HashPassword("admin"),
                        Role = Role.ADMIN
                    };

                    service.Save(tableAcc);
                }

                string deviceId = new DeviceIdBuilder()
                    //.AddProcessorId()
                    .AddSystemDriveSerialNumber()
                    //.AddMotherboardSerialNumber()
                    //.UseFormatter(new HashDeviceIdFormatter(() => SHA256.Create(), new Base64UrlByteArrayEncoder()))
                    .ToString();
                string hash = StringHelper.GetHash(deviceId);

                Application.Run(new LoginForm());
            }
        }

        private static void Service_Saved(object sender, EntityEventArgs<User> e)
        {
            System.Console.WriteLine("{0}- {1} Saved successfully.", e.Entity.Username, e.Entity.Role);
        }

        private static void BuildSchema(Configuration config)
        {
            // delete the existing db on each run
            //if (Debugger.IsAttached && File.Exists(DbFile))
            //    File.Delete(DbFile);

            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            //new SchemaExport(config)
            //  .Create(true, true);
        }

        public static void ReportCrash(Exception exception, string developerMessage = "")
        {
            var reportCrash = new ReportCrash("najib.elfarizy@gmail.com")
            {
                AnalyzeWithDoctorDump = false,
                DeveloperMessage = developerMessage,
                FromEmail = "ajieb87@gmail.com",
                UserName = "ajieb87@gmail.com",
                Password = "rjdctnypkziunrcb",
                SmtpHost = "smtp.gmail.com",
                CaptureScreen = true,
                EnableSSL = true,
                Port = 587,
            };
            reportCrash.Send(exception);
        }

        public static void ThrowException(Exception e)
        {
            MessageBox.Show(e.Message, "Exception");
        }
    }
}
