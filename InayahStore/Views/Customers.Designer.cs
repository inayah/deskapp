﻿namespace InayahStore.Views
{
    partial class Customers
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navigationGridCustomer = new InayahStore.Controls.NavigationGrid();
            this.SuspendLayout();
            // 
            // navigationGridCustomer
            // 
            this.navigationGridCustomer.AddButtonText = " Customer Baru";
            this.navigationGridCustomer.DataSourceName = null;
            this.navigationGridCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationGridCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navigationGridCustomer.Location = new System.Drawing.Point(6, 6);
            this.navigationGridCustomer.Name = "navigationGridCustomer";
            this.navigationGridCustomer.PageCount = 1;
            this.navigationGridCustomer.PageSize = 50;
            this.navigationGridCustomer.RowCount = 0;
            this.navigationGridCustomer.ShowAddButton = true;
            this.navigationGridCustomer.ShowFilterButton = true;
            this.navigationGridCustomer.Size = new System.Drawing.Size(973, 568);
            this.navigationGridCustomer.TabIndex = 10;
            this.navigationGridCustomer.UpdateDataSource += new System.Action<InayahStore.Controls.NavigationGrid.NavigationGridArgs>(this.navigationGridCustomer_UpdateDataSource);
            this.navigationGridCustomer.AddButtonClick += new System.EventHandler(this.navigationGridCustomer_AddButtonClick);
            // 
            // Customers
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.navigationGridCustomer);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Customers";
            this.Size = new System.Drawing.Size(985, 580);
            this.ResumeLayout(false);

        }

        #endregion
        private Controls.NavigationGrid navigationGridCustomer;
    }
}
