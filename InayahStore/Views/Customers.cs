﻿using DataLayer.UnitOfWorks;
using InayahStore.Forms;
using InayahStore.Helper;
using Models.Domain;
using System;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore.Views
{
    public partial class Customers : UserControl
    {
        public Customers()
        {
            InitializeComponent();
            InitializeControl();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            navigationGridCustomer.Reload();
        }

        private void InitializeControl()
        {
            var cellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleCenter };

            DataGridViewColumn[] Columns = new DataGridViewColumn[7];
            Columns[0] = new DataGridViewTextBoxColumn { DataPropertyName = "id", HeaderText = "ID", Visible = false };
            Columns[1] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "code", HeaderText = "KODE", DefaultCellStyle = cellStyle, Resizable = DataGridViewTriState.False };
            Columns[2] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "name", HeaderText = "NAMA", DefaultCellStyle = cellStyle };
            Columns[3] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "address", HeaderText = "ALAMAT", DefaultCellStyle = cellStyle };
            Columns[4] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "phone", HeaderText = "NO. TELP", DefaultCellStyle = cellStyle };
            Columns[5] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "email", HeaderText = "EMAIL", DefaultCellStyle = cellStyle };
            Columns[6] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "group", HeaderText = "GRUP", DefaultCellStyle = cellStyle };
            navigationGridCustomer.Columns = Columns;
        }

        private void navigationGridCustomer_AddButtonClick(object sender, EventArgs e)
        {

        }

        private void navigationGridCustomer_UpdateDataSource(Controls.NavigationGrid.NavigationGridArgs e)
        {
            var customers = UnitOfWork.CurrentSession.Query<Contact>().Where(c => c.ContactType == ContactType.CUSTOMER).AsQueryable();

            if (!String.IsNullOrEmpty(e.Filter))
                customers = customers.Where(c => c.Code.Contains(e.Filter) || c.Name.Contains(e.Filter));

            var paging = customers.OrderBy(c => e.OrderColumn).GetPaged(e.CurrentPage, e.PageSize);

            e.RowCount = paging.RowCount;
            e.PageCount = paging.PageCount;
            navigationGridCustomer.RefreshDataSource(new SortableBindingList<Contact>(paging.Results), e);
        }
    }
}
