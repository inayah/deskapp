﻿namespace InayahStore.Views
{
    partial class MasterData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel5 = new System.Windows.Forms.Panel();
            this.radioButtonSupplier = new System.Windows.Forms.RadioButton();
            this.radioButtonCustomer = new System.Windows.Forms.RadioButton();
            this.radioButtonProduct = new System.Windows.Forms.RadioButton();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.navigationGridProduct = new InayahStore.Controls.NavigationGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.customers1 = new InayahStore.Views.Customers();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.supplierControl = new InayahStore.Views.Suppliers();
            this.panel5.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.panel5.Controls.Add(this.radioButtonSupplier);
            this.panel5.Controls.Add(this.radioButtonCustomer);
            this.panel5.Controls.Add(this.radioButtonProduct);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(6, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1252, 67);
            this.panel5.TabIndex = 9;
            // 
            // radioButtonSupplier
            // 
            this.radioButtonSupplier.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonSupplier.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonSupplier.ForeColor = System.Drawing.Color.White;
            this.radioButtonSupplier.Image = global::InayahStore.Properties.Resources.waiter;
            this.radioButtonSupplier.Location = new System.Drawing.Point(307, 9);
            this.radioButtonSupplier.Name = "radioButtonSupplier";
            this.radioButtonSupplier.Size = new System.Drawing.Size(137, 50);
            this.radioButtonSupplier.TabIndex = 8;
            this.radioButtonSupplier.Text = " Supplier";
            this.radioButtonSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radioButtonSupplier.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radioButtonSupplier.UseVisualStyleBackColor = false;
            this.radioButtonSupplier.Click += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonCustomer
            // 
            this.radioButtonCustomer.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonCustomer.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonCustomer.ForeColor = System.Drawing.Color.White;
            this.radioButtonCustomer.Image = global::InayahStore.Properties.Resources.man;
            this.radioButtonCustomer.Location = new System.Drawing.Point(140, 9);
            this.radioButtonCustomer.Name = "radioButtonCustomer";
            this.radioButtonCustomer.Size = new System.Drawing.Size(161, 50);
            this.radioButtonCustomer.TabIndex = 7;
            this.radioButtonCustomer.Text = " Pelanggan";
            this.radioButtonCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radioButtonCustomer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radioButtonCustomer.UseVisualStyleBackColor = false;
            this.radioButtonCustomer.Click += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonProduct
            // 
            this.radioButtonProduct.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButtonProduct.Checked = true;
            this.radioButtonProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButtonProduct.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonProduct.ForeColor = System.Drawing.Color.White;
            this.radioButtonProduct.Image = global::InayahStore.Properties.Resources.box;
            this.radioButtonProduct.Location = new System.Drawing.Point(7, 9);
            this.radioButtonProduct.Name = "radioButtonProduct";
            this.radioButtonProduct.Size = new System.Drawing.Size(127, 50);
            this.radioButtonProduct.TabIndex = 6;
            this.radioButtonProduct.TabStop = true;
            this.radioButtonProduct.Text = " Barang";
            this.radioButtonProduct.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radioButtonProduct.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radioButtonProduct.UseVisualStyleBackColor = false;
            this.radioButtonProduct.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            this.radioButtonProduct.Click += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(6, 73);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1252, 607);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.navigationGridProduct);
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1244, 573);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // navigationGridProduct
            // 
            this.navigationGridProduct.AddButtonText = " Barang Baru";
            this.navigationGridProduct.DataSourceName = null;
            this.navigationGridProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationGridProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navigationGridProduct.Location = new System.Drawing.Point(3, 3);
            this.navigationGridProduct.Name = "navigationGridProduct";
            this.navigationGridProduct.PageCount = 1;
            this.navigationGridProduct.PageSize = 50;
            this.navigationGridProduct.RowCount = 0;
            this.navigationGridProduct.ShowAddButton = true;
            this.navigationGridProduct.ShowFilterButton = true;
            this.navigationGridProduct.Size = new System.Drawing.Size(1238, 567);
            this.navigationGridProduct.TabIndex = 11;
            this.navigationGridProduct.UpdateDataSource += new System.Action<InayahStore.Controls.NavigationGrid.NavigationGridArgs>(this.navigationGridProduct_UpdateDataSource);
            this.navigationGridProduct.CellClick += new System.Action<System.Windows.Forms.DataGridView, System.Windows.Forms.DataGridViewCellEventArgs>(this.navigationGridProduct_CellClick);
            this.navigationGridProduct.CellFormatting += new System.Action<System.Windows.Forms.DataGridView, System.Windows.Forms.DataGridViewCellFormattingEventArgs>(this.navigationGridProduct_CellFormatting);
            this.navigationGridProduct.AddButtonClick += new System.EventHandler(this.navigationGridProduct_AddButtonClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.customers1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1244, 581);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // customers1
            // 
            this.customers1.BackColor = System.Drawing.Color.White;
            this.customers1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customers1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customers1.Location = new System.Drawing.Point(3, 3);
            this.customers1.Name = "customers1";
            this.customers1.Size = new System.Drawing.Size(1238, 575);
            this.customers1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.supplierControl);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1244, 581);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // supplierControl
            // 
            this.supplierControl.BackColor = System.Drawing.Color.White;
            this.supplierControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.supplierControl.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierControl.Location = new System.Drawing.Point(3, 3);
            this.supplierControl.Name = "supplierControl";
            this.supplierControl.Size = new System.Drawing.Size(1238, 575);
            this.supplierControl.TabIndex = 0;
            // 
            // MasterData
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MasterData";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Size = new System.Drawing.Size(1264, 686);
            this.panel5.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton radioButtonProduct;
        private System.Windows.Forms.RadioButton radioButtonCustomer;
        private System.Windows.Forms.RadioButton radioButtonSupplier;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private Controls.NavigationGrid navigationGridProduct;
        private System.Windows.Forms.TabPage tabPage2;
        private Customers customers1;
        private System.Windows.Forms.TabPage tabPage3;
        private Suppliers supplierControl;
    }
}
