﻿using DataLayer.Repository;
using DataLayer.UnitOfWorks;
using InayahStore.Forms;
using InayahStore.Helper;
using Models.Domain;
using Services;
using System;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore.Views
{
    public partial class MasterData : UserControl
    {
        public MasterData()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            tabControl.ItemSize = new System.Drawing.Size(0, 1);

            var cellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleCenter };

            DataGridViewColumn[] Columns = new DataGridViewColumn[10];
            Columns[0] = new DataGridViewTextBoxColumn { DataPropertyName = "id", HeaderText = "ID", Visible = false };
            Columns[1] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "code", HeaderText = "KODE", DefaultCellStyle = cellStyle, Resizable = DataGridViewTriState.False };
            Columns[2] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "name", HeaderText = "NAMA", DefaultCellStyle = cellStyle };
            Columns[3] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "category", HeaderText = "KATEGORI", DefaultCellStyle = cellStyle };
            Columns[4] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "uom", HeaderText = "SATUAN", DefaultCellStyle = cellStyle };
            Columns[5] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "cost", HeaderText = "HARGA BELI", DefaultCellStyle = cellStyle };
            Columns[6] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "price", HeaderText = "HARGA JUAL", DefaultCellStyle = cellStyle };
            Columns[7] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "stock", HeaderText = "STOK", DefaultCellStyle = cellStyle };
            Columns[8] = new DataGridViewButtonColumn { SortMode = DataGridViewColumnSortMode.NotSortable, UseColumnTextForButtonValue = true, Name = "edit", HeaderText = "Edit", DefaultCellStyle = cellStyle, FillWeight = 50, Text = "Edit" };
            Columns[9] = new DataGridViewButtonColumn { SortMode = DataGridViewColumnSortMode.NotSortable, UseColumnTextForButtonValue = true, Name = "delete", HeaderText = "Delete", DefaultCellStyle = cellStyle, FillWeight = 50, Text = "Delete" };
            navigationGridProduct.Columns = Columns;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            navigationGridProduct.Reload();
        }

        private void navigationGridProduct_AddButtonClick(object sender, EventArgs e)
        {
            using (ProductInput product = new ProductInput())
            {
                DialogResult result = product.ShowDialog();
                if (result == DialogResult.OK)
                {
                    navigationGridProduct.Reload();
                }
            }
        }

        private void navigationGridProduct_UpdateDataSource(Controls.NavigationGrid.NavigationGridArgs e)
        {
            var products = UnitOfWork.CurrentSession.Query<Product>().AsQueryable();

            if (!String.IsNullOrEmpty(e.Filter))
                products = products.Where(c => c.Code.Contains(e.Filter) || c.Name.Contains(e.Filter));

            var paging = products.OrderBy(c => e.OrderColumn).GetPaged(e.CurrentPage, e.PageSize);

            e.RowCount = paging.RowCount;
            e.PageCount = paging.PageCount;
            navigationGridProduct.RefreshDataSource(new SortableBindingList<Product>(paging.Results), e);
        }

        private void navigationGridProduct_CellFormatting(DataGridView dgv, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 3:
                    var product = (Product)dgv.Rows[e.RowIndex].DataBoundItem;
                    if (product != null && product.Category != null)
                        e.Value = product.Category.Name;
                    break;
                case 5:
                case 6:
                case 7:
                    e.Value = string.Format("{0:N0}", e.Value);
                    break;
            }
        }

        private void navigationGridProduct_CellClick(DataGridView dgv, DataGridViewCellEventArgs e)
        {
            var product = (Product)dgv.Rows[e.RowIndex].DataBoundItem;
            switch (dgv.Columns[e.ColumnIndex].Name)
            {
                case "edit":

                    using (ProductInput form = new ProductInput())
                    {
                        form.Product = product;
                        DialogResult result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            navigationGridProduct.Reload();
                        }
                    }
                    break;
                case "delete":
                    if (MessageBox.Show("Hapus Barang " + product.Name + "?", "Konfirmasi", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var productRepository = new Repository<Product>();
                        EntityService<Product> service = new EntityService<Product>(productRepository);
                        service.Deleted += (sender, model) => { navigationGridProduct.Reload(); };
                        service.Delete(product);
                    }
                    break;
            }
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            switch (((Control)sender).Name)
            {
                case "radioButtonCustomer":
                    tabControl.SelectedIndex = 1;
                    break;
                case "radioButtonSupplier":
                    tabControl.SelectedIndex = 2;
                    break;
                default:
                    tabControl.SelectedIndex = 0;
                    break;
            }
        }
    }
}
