﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InayahStore
{
    public partial class MySecondCustmControl : UserControl
    {
        public MySecondCustmControl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            ps.StartPrint(WriteTxt(), "txt");
        }

        public string WriteTxt()
        {
            StringBuilder sb = new StringBuilder();
            String tou = "Yi Yin Food Company";
            String address = "No. 29, Dongmen Old Street, Luohu District, Shenzhen";
            string saleID = "2010930233330";
            String item = "item";
            decimal price = 25.00M;
            int count = 5;
            decimal total = 0.00M;
            decimal fukuan = 500.00M;
            sb.Append("            " + tou + "     \r\n");
            sb.Append("-----------------------------------------------------------------\r\n");
            sb.Append("date:" + DateTime.Now.ToShortDateString() + " " + "single number:" + saleID + "\r\n");
            sb.Append("-----------------------------------------------------------------\r\n");
            sb.Append("project" + "\t\t" + "quantity" + "\t" + "unit price" + "\t" + "subtotal" + "\r\n");
            for (int i = 0; i < count; i++)
            {
                decimal xiaoji = (i + 1) * price;
                sb.Append(item + (i + 1) + "\t\t" + (i + 1) + "\t" + price + "\t" + xiaoji);
                total += xiaoji;
                if (i != (count))
                    sb.Append("\r\n");
            }
            sb.Append("-----------------------------------------------------------------\r\n");
            sb.Append("Quantity: " + count + " Total: " + total + "\r\n");
            sb.Append("Payment: Cash" + " " + fukuan);
            sb.Append(" cash change: " + " " + (fukuan - total) + "\r\n");
            sb.Append("-----------------------------------------------------------------\r\n");
            sb.Append("Address:" + address + "\r\n");
            sb.Append("phone:123456789 123456789\r\n");
            sb.Append(" Thank you for your welcome to visit next time");
            return sb.ToString();
        }
    }
}
