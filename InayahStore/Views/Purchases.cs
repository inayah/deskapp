﻿using DataLayer.Repository;
using DataLayer.UnitOfWorks;
using InayahStore.Forms;
using InayahStore.Helper;
using Models.Domain;
using NHibernate.Criterion;
using Services;
using System;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore.Views
{
    public partial class Purchases : UserControl
    {
        private Transaction sales;

        public Purchases()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            tabControl.ItemSize = new System.Drawing.Size(0, 1);

            var cellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleCenter };

            DataGridViewColumn[] Columns = new DataGridViewColumn[6];
            Columns[0] = new DataGridViewTextBoxColumn { DataPropertyName = "id", HeaderText = "ID", Visible = false };
            Columns[1] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "code", HeaderText = "KODE", DefaultCellStyle = cellStyle, Resizable = DataGridViewTriState.False };
            Columns[2] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "paper_date", HeaderText = "TANGGAL", DefaultCellStyle = cellStyle };
            Columns[3] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "customer", HeaderText = "PELANGGAN", DefaultCellStyle = cellStyle };
            Columns[4] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "status", HeaderText = "STATUS", DefaultCellStyle = cellStyle };
            Columns[5] = new DataGridViewComboBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "action", HeaderText = "#", DefaultCellStyle = cellStyle, Width = 50, ReadOnly = false, DataSource = new string[] { "Edit", "Delete" } };
            navigationGridSales.Columns = Columns;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            navigationGridSales.Reload();
        }

        private void navigationGridUser_AddButtonClick(object sender, EventArgs e)
        {
            using (UserInput user = new UserInput())
            {
                DialogResult result = user.ShowDialog();
                if (result == DialogResult.OK)
                {
                    navigationGridSales.Reload();
                }
            }
        }

        private void navigationGridSales_UpdateDataSource(Controls.NavigationGrid.NavigationGridArgs e)
        {
            var orders = UnitOfWork.CurrentSession.Query<Transaction>().Where(o => o.Type == TransactionType.SALES).AsQueryable();

            if (!String.IsNullOrEmpty(e.Filter))
                orders = orders.Where(c => c.Contact.Name.Contains(e.Filter));

            var paging = orders.OrderBy(c => e.OrderColumn).GetPaged(e.CurrentPage, e.PageSize);

            e.RowCount = paging.RowCount;
            e.PageCount = paging.PageCount;
            navigationGridSales.RefreshDataSource(new SortableBindingList<Transaction>(paging.Results), e);
        }

        private void navigationGridUser_CellFormatting(DataGridView dataGrid, DataGridViewCellFormattingEventArgs e)
        {
            sales = (Transaction)dataGrid.Rows[e.RowIndex].DataBoundItem;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            switch (((Control)sender).Name)
            {
                case "radioButtonEntry":
                    initSalesEntry();
                    break;
                default:
                    tabControl.SelectedIndex = 0;
                    break;
            }
        }

        private void initSalesEntry()
        {
            tabControl.SelectedIndex = 1;
            var orders = UnitOfWork.CurrentSession.QueryOver<Transaction>().Select(
                Projections.ProjectionList().Add(Projections.Max<Transaction>(x => x.Code))
            ).Where(o => o.Type == TransactionType.SALES)
            .List<String>().First();

            textBoxCode.Text = StringHelper.NextCode("PJ-", orders, 4);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var contactRepository = new Repository<Transaction>();
            EntityService<Transaction> service = new EntityService<Transaction>(contactRepository);
            service.Saved += onCompany_Saved;
            service.Save(sales);
        }

        private void onCompany_Saved(object sender, EntityEventArgs<Transaction> e)
        {

            MessageBox.Show("Perubahan berhasil disimpan", "Success");
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {

        }

    }
}
