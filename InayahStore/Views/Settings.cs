﻿using DataLayer.Repository;
using DataLayer.UnitOfWorks;
using InayahStore.Forms;
using InayahStore.Helper;
using Models.Domain;
using Services;
using System;
using System.Linq;
using System.Windows.Forms;

namespace InayahStore.Views
{
    public partial class Settings : UserControl
    {
        public AppEvent AppEvent;
        private Contact company;

        public Settings()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            tabControl.ItemSize = new System.Drawing.Size(0, 1);

            var cellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleCenter };

            DataGridViewColumn[] Columns = new DataGridViewColumn[5];
            Columns[0] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "username", HeaderText = "USERNAME", DefaultCellStyle = cellStyle, FillWeight = 100 };
            Columns[1] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "role", HeaderText = "HAK AKSES", DefaultCellStyle = cellStyle, FillWeight = 100 };
            Columns[2] = new DataGridViewTextBoxColumn { SortMode = DataGridViewColumnSortMode.Automatic, DataPropertyName = "status", HeaderText = "STATUS", DefaultCellStyle = cellStyle, FillWeight = 100 };
            Columns[3] = new DataGridViewButtonColumn { SortMode = DataGridViewColumnSortMode.NotSortable, UseColumnTextForButtonValue = true, Name = "edit", HeaderText = "Edit", DefaultCellStyle = cellStyle, FillWeight = 50, Text = "Edit" };
            Columns[4] = new DataGridViewButtonColumn { SortMode = DataGridViewColumnSortMode.NotSortable, UseColumnTextForButtonValue = true, Name = "delete", HeaderText = "Delete", DefaultCellStyle = cellStyle, FillWeight = 50, Text = "Delete" };
            navigationGridUser.Columns = Columns;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initializeCompany();
            navigationGridUser.Reload();
        }

        private void initializeCompany()
        {
            company = Program.Company;
            pictureBoxLogo.Image = ImageHelper.FromByteArray(company.Image, Properties.Resources.empty);
            textBoxName.Text = company.Name;
            textBoxAddress.Text = company.Address;
            textBoxEmail.Text = company.Email;
            textBoxPhone.Text = company.Phone;
            textBoxFax.Text = company.Fax;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            switch (((Control)sender).Name)
            {
                case "radioButtonUser":
                    tabControl.SelectedIndex = 1;
                    break;
                default:
                    tabControl.SelectedIndex = 0;
                    break;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            company.Image = ImageHelper.ToByteArray(pictureBoxLogo.Image);
            company.Name = textBoxName.Text;
            company.Address = textBoxAddress.Text;
            company.Email = textBoxEmail.Text;
            company.Phone = textBoxPhone.Text;
            company.Fax = textBoxFax.Text;

            var contactRepository = new Repository<Contact>();
            EntityService<Contact> service = new EntityService<Contact>(contactRepository);
            service.Saved += onCompany_Saved;
            service.Save(company);
        }

        private void onCompany_Saved(object sender, EntityEventArgs<Contact> e)
        {
            UnitOfWork.Current.Flush();
            if (AppEvent != null) AppEvent.OnCompanyChanged(e.Entity);

            MessageBox.Show("Perubahan berhasil disimpan", "Success");
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            initializeCompany();
        }

        private void buttonLogo_Click(object sender, EventArgs e)
        {
            var option = openFileDialog.ShowDialog();
            if (option == DialogResult.OK)
            {
                var file = openFileDialog.OpenFile();
                pictureBoxLogo.ImageLocation = openFileDialog.FileName;
            }
        }

        private void navigationGridUser_AddButtonClick(object sender, EventArgs e)
        {
            using (UserInput user = new UserInput())
            {
                DialogResult result = user.ShowDialog();
                if (result == DialogResult.OK)
                {
                    navigationGridUser.Reload();
                }
            }
        }

        private void navigationGridUser_UpdateDataSource(Controls.NavigationGrid.NavigationGridArgs e)
        {
            var users = UnitOfWork.CurrentSession.Query<User>().AsQueryable();

            if (!String.IsNullOrEmpty(e.Filter))
                users = users.Where(c => c.Username.Contains(e.Filter));

            var paging = users.OrderBy(c => e.OrderColumn).GetPaged(e.CurrentPage, e.PageSize);

            e.RowCount = paging.RowCount;
            e.PageCount = paging.PageCount;
            navigationGridUser.RefreshDataSource(new SortableBindingList<User>(paging.Results), e);
        }

        private void navigationGridUser_CellFormatting(DataGridView dataGrid, DataGridViewCellFormattingEventArgs e)
        {
            var user = (User)dataGrid.Rows[e.RowIndex].DataBoundItem;
        }

        private void navigationGridUser_CellClick(DataGridView dgv, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;

            var row = dgv.Rows[e.RowIndex];
            var user = (User)row.DataBoundItem;
            if (user == null) return;

            switch (dgv.Columns[e.ColumnIndex].Name)
            {
                case "edit":

                    using (UserInput form = new UserInput())
                    {
                        form.User = user;
                        DialogResult result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            navigationGridUser.Reload();
                        }
                    }
                    break;
                case "delete":
                    if (MessageBox.Show("Hapus User " + user.Username + "?", "Konfirmasi", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var userRepository = new Repository<User>();
                        EntityService<User> service = new EntityService<User>(userRepository);
                        service.Deleted += User_Deleted;
                        service.Delete(user);
                    }
                    break;
            }
        }

        private void User_Deleted(object sender, EntityEventArgs<User> e)
        {
            UnitOfWork.Current.TransactionalFlush();
            navigationGridUser.Reload();
        }
    }
}
