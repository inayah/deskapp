﻿namespace InayahStore.Views
{
    partial class Suppliers
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navigationGridSupplier = new InayahStore.Controls.NavigationGrid();
            this.SuspendLayout();
            // 
            // navigationGridCustomer
            // 
            this.navigationGridSupplier.AddButtonText = " Supplier Baru";
            this.navigationGridSupplier.DataSourceName = null;
            this.navigationGridSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationGridSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navigationGridSupplier.Location = new System.Drawing.Point(6, 6);
            this.navigationGridSupplier.Name = "navigationGridCustomer";
            this.navigationGridSupplier.PageCount = 1;
            this.navigationGridSupplier.PageSize = 50;
            this.navigationGridSupplier.RowCount = 0;
            this.navigationGridSupplier.ShowAddButton = true;
            this.navigationGridSupplier.ShowFilterButton = true;
            this.navigationGridSupplier.Size = new System.Drawing.Size(973, 568);
            this.navigationGridSupplier.TabIndex = 10;
            this.navigationGridSupplier.UpdateDataSource += new System.Action<InayahStore.Controls.NavigationGrid.NavigationGridArgs>(this.navigationGridSupplier_UpdateDataSource);
            this.navigationGridSupplier.AddButtonClick += new System.EventHandler(this.navigationGridSupplier_AddButtonClick);
            // 
            // Suppliers
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.navigationGridSupplier);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Suppliers";
            this.Size = new System.Drawing.Size(985, 580);
            this.ResumeLayout(false);

        }

        #endregion
        private Controls.NavigationGrid navigationGridSupplier;
    }
}
