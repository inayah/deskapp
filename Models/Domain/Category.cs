﻿using System.Collections.Generic;

namespace Models.Domain
{
    public class Category
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

        public virtual IList<Product> Products { get; set; }
    }
}
