﻿namespace Models.Domain
{
    public class Contact
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Email { get; set; }
        public virtual ContactType ContactType { get; set; }
        public virtual  ContactGroup Group { get; set; }
        public virtual byte[] Image { get; set; }

    }
}
