﻿using System.Collections.Generic;

namespace Models.Domain
{
    public class ContactGroup
    {
        public virtual int Id { get; set; }
        public virtual string GroupName { get; set; }
        public virtual IList<Contact> Contact { get; set; }
    }
}
