﻿namespace Models.Domain
{
    public enum ContactType
    {      
        COMPANY,
        CUSTOMER,
        SUPPLIER,
        OWNER
    }
}
