﻿namespace Models.Domain
{
    public enum PaidStatus
    {
        UNPAID,
        PARTIAL,
        PAID,
    }
}
