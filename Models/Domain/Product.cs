﻿namespace Models.Domain
{
    public class Product
    {

        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Barcode { get; set; }
        public virtual string Name { get; set; }
        public virtual string Uom { get; set; }
        public virtual Category Category { get; set; }
        public virtual decimal Cost { get; set; }
        public virtual decimal Markup { get; set; }
        public virtual decimal Price { get; set; }
        public virtual decimal Tax { get; set; }

    }
}
