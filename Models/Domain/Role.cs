﻿namespace Models.Domain
{
    public enum Role
    {
        ADMIN = 1,
        CASHIER = 2,
    }
}
