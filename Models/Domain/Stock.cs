﻿using System;

namespace Models.Domain
{
    public class Stock
    {
        public virtual int Id { get; set; }
        public virtual string Sku { get; set; }
        public virtual double Open { get; set; }
        public virtual double InQty { get; set; }
        public virtual double Out { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual Product Product { get; set; }
    }
}
