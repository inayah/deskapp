﻿using Models.Common;
using System;
using System.Collections.Generic;

namespace Models.Domain
{
    public class Transaction
    {
        public virtual int Id { get; set; }
        public virtual DateTime PaperDate { get; set; }
        public virtual DateTime DueDate { get; set; }
        public virtual string Code { get; set; }
        public virtual string RefNumber { get; set; }
        public virtual string Note { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal Disc { get; set; }
        public virtual decimal Tax { get; set; }
        public virtual decimal SubTotal { get; set; }
        public virtual TransactionStatus Status { get; set; }
        public virtual PaidStatus PaidStatus { get; set; }
        public virtual TransactionType Type { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual string ModifiedBy { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual IList<TransactionItem> Items { get; set; }
    }
}
