﻿using System;
using System.Collections.Generic;

namespace Models.Domain
{
    public class TransactionItem
    {
        public virtual int Id { get; set; }
        public virtual string Sku { get; set; }
        public virtual string Description { get; set; }
        public virtual string Uom { get; set; }
        public virtual int Quantity { get; set; }
        public virtual decimal Price { get; set; }
        public virtual decimal Disc { get; set; }
        public virtual decimal Tax { get; set; }
        public virtual decimal SubTotal { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual Product Product { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
