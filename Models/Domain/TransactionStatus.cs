﻿namespace Models.Domain
{
    public enum TransactionType
    {
        SALES,
        SALESORDER,
        SALESRETURN,

        PURCHASE,
        PURCHASEORDER,
        PURCHASERETURN,

        INVENTORY
    }
}
