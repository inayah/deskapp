﻿namespace Models.Domain
{
    public enum TransactionStatus
    {
        DRAFT,
        TEMPRORY,
        ACCEPTED,
        CLOSED
    }
}
