﻿using System;

namespace Models.Domain
{
    public class User
    {
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual Role Role { get; set; }
        public virtual UserStatus Status { get; set; }
        public virtual bool IsActive { get { return Status == UserStatus.ENABLED; } }
        public virtual byte[] Avatar { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime ModifiedAt { get; set; }

    }
}
