﻿namespace Models.Domain
{
    public enum UserStatus
    {
        DISABLED = 0,
        ENABLED = 1,
    }
}
