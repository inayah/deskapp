﻿using FluentNHibernate.Mapping;
using Models.Domain;

namespace Models.Mappings
{
    public class CategoryMap : ClassMap<Category>
    {
        public CategoryMap()
        {

            Table("Category");

            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(x => x.Code);
            Map(x => x.Name);
            Map(x => x.Description);

            HasMany<Product>(x => x.Products)
                .KeyColumn("ProductFk")
                .Cascade.All();
        }
    }
}
