﻿using FluentNHibernate.Mapping;
using Models.Domain;

namespace Models.Mappings
{
    public class ContactGroupMap : ClassMap<ContactGroup>
    {
        public ContactGroupMap() {
            Table("ContactGroup");
          
            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(x => x.GroupName);

            HasMany(x => x.Contact)
                .KeyColumn("GroupFk")
                .Cascade.SaveUpdate();
        }
    }
}
