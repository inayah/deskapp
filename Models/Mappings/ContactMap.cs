﻿using FluentNHibernate.Mapping;
using Models.Domain;

namespace Models.Mappings
{
    public class ContactMap : ClassMap<Contact>
    {
        public ContactMap() {
            Table("Contact");

            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(x => x.Name);
            Map(x => x.Address);
            Map(x => x.Email);
            Map(x => x.Fax);
            Map(x => x.Image);
            Map(x => x.ContactType);
            Map(x => x.Phone);
            Map(x => x.Code);

            References<ContactGroup>(x => x.Group)
                .Column("GroupFk");

        }
    }
}
