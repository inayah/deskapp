﻿using FluentNHibernate.Mapping;
using Models.Domain;

namespace Models.Mappings
{
    public class ProductMap : ClassMap<Product>
    {
        public ProductMap() {
            Table("Products");
            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(x => x.Code);
            Map(x => x.Barcode);
            Map(x => x.Name).Length(255).Not.Nullable();
            Map(x => x.Uom);
            Map(x => x.Cost);
            Map(x => x.Price);
            Map(x => x.Markup);
            Map(x => x.Tax);

            References<Category>(x => x.Category).Column("CategoryFk");
        }
    }
}
