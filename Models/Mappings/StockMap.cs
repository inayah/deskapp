﻿using FluentNHibernate.Mapping;
using Models.Domain;

namespace Models.Mappings
{
    public class StockMap : ClassMap<Stock>
    {
        public StockMap() {

            Table("Stocks");

            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(q => q.Sku);
            Map(q => q.Open);
            Map(q => q.InQty);
            Map(q => q.Out);

            Map(x => x.CreatedAt);
            Map(x => x.ModifiedAt);

            References<Product>(x => x.Product).Column("ProductFk");
        }
    }
}
