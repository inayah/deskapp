﻿using FluentNHibernate.Mapping;
using Models.Common;
using Models.Domain;

namespace Models.Mappings
{
    public class TransactionItemMap : ClassMap<TransactionItem>
    {
        public TransactionItemMap() {

            Table("TransactionItems");
           
            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(x => x.Sku);
            Map(x => x.Description);
            Map(x => x.Uom);
            Map(x => x.Quantity);
            Map(x => x.Price);
            Map(x => x.Disc);
            Map(x => x.Tax);
            Map(x => x.SubTotal);
            Map(x => x.CreatedAt);
            Map(x => x.ModifiedAt);

            References<Product>(x => x.Product).Column("ProductFk");
            References<Transaction>(x => x.Transaction).Column("TransactionFk");
        }
    }
}
