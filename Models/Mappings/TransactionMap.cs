﻿using FluentNHibernate.Mapping;
using Models.Common;
using Models.Domain;

namespace Models.Mappings
{
    public class TransactionMap : ClassMap<Transaction>
    {
        public TransactionMap() {

            Table("Transactions");
           
            Id(x => x.Id)
                .Column("Id")
                .Not.Nullable()
                .GeneratedBy.Identity();

            Map(x => x.Code);
            Map(x => x.RefNumber);
            Map(x => x.PaperDate);
            Map(x => x.DueDate);
            Map(x => x.Note);
            Map(x => x.Total);
            Map(x => x.Disc);
            Map(x => x.Tax);
            Map(x => x.SubTotal);
            Map(x => x.Status);
            Map(x => x.PaidStatus);
            Map(x => x.Type);
            Map(x => x.CreatedAt);
            Map(x => x.CreatedBy);
            Map(x => x.ModifiedAt);
            Map(x => x.ModifiedBy);

            References<Contact>(x => x.Contact).Column("ContactFk");
            References<Currency>(x => x.Currency).Column("CurrencyFk");

            HasMany<TransactionItem>(x => x.Items)
                .KeyColumn("TransactionFk")
                .Cascade.All();
        }
    }
}
