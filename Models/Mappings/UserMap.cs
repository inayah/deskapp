﻿using FluentNHibernate.Mapping;
using Models.Domain;

namespace Models.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap() {
            Table("Users");
            Id(x => x.Id);
            Map(x => x.Username).Length(100).Not.Nullable();
            Map(x => x.Password);
            Map(x => x.Role);
            Map(x => x.Status);
            Map(x => x.Avatar).Length(int.MaxValue);
            Map(x => x.CreatedAt);
            Map(x => x.ModifiedAt);
        }
    }
}
