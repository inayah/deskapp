﻿using System;

namespace Services
{
    public class EntityEventArgs<T> :EventArgs
    {
        public Exception Exception { get; set; }
        public T Entity { get; set; }
    }
}
